% function makePlots
close all;



current_min = 0;
current_gap = 0.1;
current_step = 50;
energy = [5,10,20,30,40,50,60];
directTOF = [0.81813,0.578509,0.409074,0.334012,0.289267,0.258732,0.236193];
electronNum = 1000;
mcpPosition = 650;
dt = 0.0004; %msec
time = 1; %msec
dataFolder = 'output';
%
energyNum = length(energy);
currentList = [current_min:current_gap:current_min+current_gap*current_step];
currentNum = current_step+1;
timeAxis = [0:dt:time];
%read data
TOF = zeros(currentNum,electronNum,energyNum);
Xend = zeros(currentNum,electronNum,energyNum);
for i=1:currentNum
    filename = fullfile(dataFolder,[num2str(currentList(i)),'.txt']);
    disp(['read in ',filename]);
    [~,~,~,~,~,~,~,TOFTemp,XendTemp] = importfile(filename);
    TOF(i,:,:) = reshape(TOFTemp,[electronNum,energyNum]);
    Xend(i,:,:) = reshape(XendTemp,[electronNum,energyNum]);
end

%get Tof and collect effiency
tof = zeros(currentNum,length(timeAxis));
effi = zeros(currentNum,energyNum);
for i=1:currentNum
    for j=1:energyNum
        %check if the particle hit the MCP
        xData = squeeze(Xend(i,:,j));
        index = find(xData>mcpPosition-1 & xData<=mcpPosition);
        effi(i,j) = length(index)/electronNum;
        index2 = ceil(squeeze(TOF(i,index,j))/dt);
        for k=1:length(index2)
            tof(i,index2(k)) = tof(i,index2(k)) + 1;
        end
    end
end
%get full width of TOF
dtt = zeros(currentNum,energyNum);
errorFunc = cell(currentNum,1);
%get peak range index for each energy
index = zeros(1,energyNum);
indexPeak = zeros(2,energyNum);
for i=1:energyNum
    [~,index(i)] = min(abs(directTOF(i)-timeAxis));
end
indexPeak(1,:) = index-round(0.01/dt);
indexPeak(2,:) = indexPeak(1,:) + round(0.03/dt);
for i=1:currentNum
%     figure;
    for j=1:energyNum
        %fit each peak with gaussian function
        data = squeeze(tof(i,indexPeak(1,j):indexPeak(2,j))/electronNum);
        timeRange = timeAxis(indexPeak(1,j):indexPeak(2,j));
        [x,y] = prepareCurveData(timeRange,data);
        func = fit(x,y,'gauss1');
        func.c1;
        dtt(i,j) = func.c1*1.6651/directTOF(j);
%         subplot(211);
%         hold all;
%         plot(func,x,y)
    end
    %fit the fwhm with poly2
    [x,y] = prepareCurveData(energy,(1./(1+dtt(i,:))).^2-1);
    errorFunc{i} = fit(x,y,'poly1');
%     subplot(212);
%     plot(errorFunc{i},x,y)
end

e=[0:0.1:100];
err = zeros(currentNum,length(e));
for i=1:currentNum
    func = errorFunc{i};
    err(i,:) =  func(e);
end
figure('Name','Resolution','color',[1 1 1]);
set(gca,'fontsize',18);
imagesc(e,currentList,abs(err)*100);
colormap(jet(256));
set(gca,'ydir','normal');
xlabel('Kinetic energy [eV]');
ylabel('Solenoid current [A]');
c=colorbar;
set(c,'fontsize',16);
ylabel(c,'Resolution [%]');
caxis([0,5]);

%plot time of flight
currentPlot = 1;
[~,currentIndex] = min(abs(currentPlot-currentList));
figure('Name','Time of flight','color',[1 1 1]);
set(gca,'fontsize',18);
plot(timeAxis*1000,tof(currentIndex,:)/electronNum);
hold on;
for i=1:energyNum
   plot([directTOF(i),directTOF(i)]*1000,get(gca,'ylim'),'--k'); 
end
xlabel('Time of flight [ns]');
ylabel('Detect efficiency');
title(['Solenoid current = ',num2str(currentList(currentIndex)),' A']);



%plot time of flight map
figure('Name','Time of flight','color',[1 1 1]);
set(gca,'fontsize',18);
imagesc(timeAxis*1000,currentList,tof/electronNum)
set(gca,'ydir','normal');
% title('Time of flight');
xlabel('Time of flight [ns]');
ylabel('Solenoid current [A]');
set(gca,'xtick',[0:200:1000]);
cmap = colormap(jet(128));
cmap(1,:) = [1 1 1];
colormap(cmap);
c=colorbar;
set(c,'fontsize',16);
ylabel(c,'Detect efficiency');

%plot detect effiency
figure('Name','Detect effiency','color',[1 1 1]);
set(gca,'fontsize',18);
hold all;
str=[];
for i=1:energyNum
    plot(currentList,smooth(effi(:,i),3)); str{i,1} = ['E = ',num2str(energy(i)),' eV'];
end
legend(str,'location','EastOutside');
legend boxoff;
box on;
xlabel('Solenoid current [A]');
ylabel('Detect efficiency');












