-- solenoid.lua - SIMION workbench program that
-- incorporates solenoid magnetic field from Biot-Savart
-- calculation into workbench.
--
-- The workbench must contain an empty
-- magnetic PA instance in which to apply this magnetic field.
--

-- ------ Begin Next Fly'm ------
-- 1	0.81813	5
-- 2	0.578509	10
-- 3	0.409074	20
-- 4	0.334012	30
-- 5	0.289267	40
-- 6	0.258732	50
-- 7	0.236193	60



simion.workbench_program()

-- Load Biot-Savart magnetic field calculation support.
local MField = require "simionx.MField"

--initilize solenoid magnetic field.
local field = MField.solenoid_hoops {
	current = 2,
	first   = MField.vector(-350,0,0),
	last    = MField.vector(650,0,0),
	radius  = 35,
	nturns  = 1000,
}
-- Draw coils in SIMION 8.1
field:draw()

-- Override magnetic field in magnetic PA instances
-- with that in the field object.
function segment.mfield_adjust()
  ion_bfieldx_gu, ion_bfieldy_gu, ion_bfieldz_gu =
    field(ion_px_mm, ion_py_mm, ion_pz_mm)
end



