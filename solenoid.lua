-- solenoid.lua - SIMION workbench program that
-- incorporates solenoid magnetic field from Biot-Savart
-- calculation into workbench.
--
-- The workbench must contain an empty
-- magnetic PA instance in which to apply this magnetic field.
--


simion.workbench_program()

-- Load Biot-Savart magnetic field calculation support.
local MField = require "simionx.MField"

-- current range
local currentNow
adjustable current_min = 0
adjustable current_gap = 0.1
adjustable current_step = 50


--initilize solenoid magnetic field.
local field = MField.solenoid_hoops {
	current = 0,
	first   = MField.vector(-350,0,0),
	last    = MField.vector(650,0,0),
	radius  = 35,
	nturns  = 1000,
}
-- Draw coils in SIMION 8.1
field:draw()


--
local total_number = 0
local energy={}
local angle_el={}
local angle_az={}
local x={}
local y={}
local tob={}
function segment.initialize()
	local speed, az, el = rect3d_to_polar3d(ion_vx_mm, ion_vy_mm, ion_vz_mm)
	local ke = speed_to_ke(speed, ion_mass)
	table.insert(energy, ke)
	table.insert(angle_el, el)
	table.insert(angle_az, az)
	table.insert(x, ion_px_mm)
	table.insert(y, ion_py_mm)
	table.insert(tob,ion_time_of_birth)
	total_number = total_number+1
end

--dump the parameters
local cycle_now = 0
local cycle_steps = current_step
print('loops:', cycle_steps)
--time recorder
local tStart,tStop,tUsed

function segment.flym()
	tUsed = 0
	--trajectory, No, No
	sim_trajectory_image_control = 3
	--trajectory_quality
	sim_trajectory_quality = 3
	
	--scan the current
	for i = 0, cycle_steps do
	currentNow = current_min + i*current_gap
	--count and record time
		cycle_now = cycle_now + 1
		tStart= os.clock()
		print('step:', cycle_now, 'current: ', currentNow)
		--set solenoid magnetic field.	
		field = MField.solenoid_hoops {
			current = currentNow,
			first   = MField.vector(-650,0,0),
			last    = MField.vector(650,0,0),
			radius  = 50,
			nturns  = 650,
		}
		-- run
		run()
		--dump the data to file
		Dump()
		--record the stop time
		tStop = os.clock()
		tUsed = tStop-tStart
	end
end



local xend = {}
local tof = {}
--called just after all ions have died
function segment.terminate()
	table.insert(xend, ion_px_mm)
	table.insert(tof, ion_time_of_flight)
end


--called just after any terminate segment calls
function segment.terminate_run()
	local message
	message = tostring(cycle_steps).. "/" ..tostring(cycle_steps-cycle_now).."    ".. 
			"Step time: " .. string.format('%5.2f', tUsed) .. " S    "..
			"Time left:" .. string.format('%8.1f',(cycle_steps-cycle_now)*tUsed/60).. " minutes"
	simion.status(message)
end


-- Override magnetic field in magnetic PA instances
-- with that in the field object.
function segment.mfield_adjust()
  ion_bfieldx_gu, ion_bfieldy_gu, ion_bfieldz_gu =
    field(ion_px_mm, ion_py_mm, ion_pz_mm)
end




--dump the data to files
function Dump()
	local filepath = "output"
	if cycle_now == 1 then
		os.execute( "rmdir /s /q " .. filepath)
		os.execute( "mkdir " .. filepath)
	end
	local ofilename = filepath.."\\"..tostring(currentNow) .. ".txt"
	local ofile = io.open(ofilename, "w+")
	
	print('create file ',ofilename, 'particle number: ',total_number/2)
	ofile:write("NO",'\t',"TOB",'\t',"Energy",'\t',"Angle_El",'\t',"Angle_Az",'\t',"X",'\t',"Y",'\t',"TOF",'\t',"XEND",'\n')
	for id = 1, total_number,2 do
		if tof[id] == nil then
			tof[id] = 0
			xend[id] = 0
		end
		ofile:write(id,'\t',tob[id],'\t',energy[id],'\t',angle_el[id],'\t',angle_az[id],'\t',x[id],'\t',y[id],'\t',tof[id],'\t',xend[id],'\n')
	end
	ofile:close()

	--reset the data
	for ci = 1, total_number do
		angle_el[ci]=nil
		angle_az[ci]=nil
		x[ci]=nil
		y[ci]=nil
		energy[ci]=nil
		tob[ci]=nil
		xend[ci]=nil
		tof[ci]=nil
	end	
	total_number = 0
end
